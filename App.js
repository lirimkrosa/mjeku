import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import Navigation from './src/Components/Navigation';
import Provider from './src/Components/index';
export default function App() {
  return (
    <Provider>
      <Navigation />
    </Provider>
  );

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
