import React from 'react';
import { StyleSheet, Text, View,Dimensions,TextInput,Image,KeyboardAvoidingView} from 'react-native';
import {ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Global from '../Components/Global';
import MapView, { Marker } from 'react-native-maps';
import RNPickerSelect from 'react-native-picker-select';

var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;

export default function Porfile({navigation}) {
    const context = React.useContext(Global);
    const placeholder = {
        label: 'Zgjedh Kategorine',
        value: null,
        color: '#FFFFFF',
      };
    const valider = () => {
        
        context.setNotification({type: 'SUCCESS', message:'Ruajtur me sukses'})
    }
    return (     

        <View style={styles.container}>
                 <ScrollView contentContainerStyle={{flexGrow:0}} style={{height:'100%', width:'100%'}}>
                     <View >
                       <View style={styles.TopHandle}>
                            <View>
                                <TouchableOpacity style={styles.triangle} onPress={() => navigation.goBack()}></TouchableOpacity>
                            </View> 
                        </View> 
                        <KeyboardAvoidingView style={{width:'100%', marginLeft:'5%', flex:1}} behavior={Platform.OS === 'ios' ? 'padding' : undefined}> 
                            <Text style={styles.textInput}>Emri Ordinances</Text>
                            <TextInput  style={styles.input} placeholder="P.sh Dhembi" onChangeText={null}></TextInput>
                            <Text style={styles.textInput}>Kategoria</Text>
                                <RNPickerSelect 
                                    onValueChange={(value) => console.log(value)}
                                    items={context.kategorite}
                                    placeholder={placeholder}
                                    style={{
                                        ...pickerSelectStyles,
                                        placeholder: {
                                          color: '#127489',
                                          fontSize: 15,
                                          fontWeight: 'bold',
                                        },
                                      }}
                                />
                            <Text style={styles.textInput}>Numri Telefonit</Text> 
                            <TextInput  style={styles.input} placeholder="P.sh +38344******" ></TextInput>
                            <Text style={styles.textInput}>Kordinatat</Text>
                            <View style={{height:'65%'}}>
                                <MapView
                                        style={styles.map}
                                        initialRegion={{
                                        latitude: 42.6617355,
                                        longitude: 21.1381487,
                                        latitudeDelta: 0.0922,
                                        longitudeDelta: 0.0421,
                                        }}
                                        >
                                    <Marker coordinate = {{latitude: 42.6617394,longitude: 21.1663333}}
                                    draggable
                                    pinColor = {"purple"}
                                    title={"Dhembi"}
                                    description={"Stomatologji"}/>
                                </MapView>
                            </View>
                            <TouchableOpacity style={styles.button} onPress={() => valider()}>
                                <Text style={styles.KycuButton}>Ruaj</Text>
                            </TouchableOpacity> 
                            </KeyboardAvoidingView>
                            </View>
                </ScrollView>
                       
                </View>

      );

}
const styles = StyleSheet.create({
    container: {
        width:Width,
        height:Height,
        
        backgroundColor: '#fff',
        flex: 1,
        justifyContent: 'space-around',
        alignItems: 'center'
    },
    map: {
        width:'90%',
        height:'70%',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        borderRadius:20
      },
    TopHandle:{
        justifyContent:'space-between', 
        flexDirection:'row', 
        width:"90%", 
        marginTop:60,
        marginLeft:15,
        marginBottom:25,
        marginLeft:'6%'
    },
    Kycu:{
        marginTop:70,
        fontWeight:'bold',
        fontSize:25,
        color:'#636363'
    },
    KycuButton:{
        fontSize:20,
        fontWeight:'bold',
        color:'#127489',
        width:'70%',
        marginLeft:'40%'
    },
    input:{
        backgroundColor: '#F3F3F3',
        borderRadius: 15,
        width:'90%',
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 15,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
      },
      button:{
        justifyContent:'center',
        alignContent:'center',
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        borderRadius: 15,
        width:"90%",
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
    },
    textInput:{
        width:'70%',
        marginTop:10,
        fontWeight:'bold',
        fontSize:22,
        color:'#636363',
        marginLeft:20
    },

    triangle: {
            width: 0,
            height: 0,
            backgroundColor: 'transparent',
            borderStyle: 'solid',
            borderTopWidth: 0,
            borderRightWidth: 10,
            borderBottomWidth: 20,
            borderLeftWidth: 10,
            borderTopColor: 'black',
            borderRightColor: 'transparent',
            borderBottomColor: '#636363',
            borderLeftColor: 'transparent',
            transform: [
            {rotate: '-90deg'}]
    },
  });

  const pickerSelectStyles = StyleSheet.create({
    inputIOS: {
        justifyContent:'center',
        alignContent:'center',
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        borderRadius: 15,
        width:Width-40,
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 20,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
    },
    inputAndroid: {
        justifyContent:'center',
        alignContent:'center',
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        borderRadius: 15,
        width:Width-40,
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 20,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
    },
  });