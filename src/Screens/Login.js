import React from 'react';
import { StyleSheet, Text, View,Dimensions,TextInput,Image,KeyboardAvoidingView} from 'react-native';
import {TouchableOpacity } from 'react-native-gesture-handler';
import Global from '../Components/Global'
var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;

export default function Login({navigation}) {
    const context = React.useContext(Global);
    const valider = () => {
        
        context.setNotification({type: 'SUCCESS', message:'Autorizuar me sukses'})
        navigation.navigate('Ballina')
    }
    return (      
                <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                        <View style={styles.TopHandle}>
                            <View>
                                <TouchableOpacity style={styles.triangle} onPress={() => navigation.goBack()}></TouchableOpacity>
                                <Text style={styles.Kycu}>KYCU</Text>
                            </View>                    
                            <Image style={{width: 180, height: 200}} source={{uri: 'https://i.imgur.com/2Q58q0k.png'}}/>  
                        </View>
                        <KeyboardAvoidingView style={{width:'100%', marginLeft:'10%', flex:0.1}} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                            <Text style={styles.textInput}>E-MAIL</Text>
                            <TextInput  style={styles.input} placeholder="psh. emri@domain.com" onChangeText={null}></TextInput>
                            <Text style={styles.textInput}>PASSWORD</Text> 
                            <TextInput  style={styles.input} placeholder="********" secureTextEntry={true} ></TextInput>
                            <TouchableOpacity style={styles.button} onPress={() => valider()}>
                                <Text style={styles.KycuButton}>KYCU</Text>
                            </TouchableOpacity> 
                        </KeyboardAvoidingView>
                </KeyboardAvoidingView>

      );

}
const styles = StyleSheet.create({
    container: {
    width:Width,
    height:Height,
      backgroundColor: '#fff',
      alignItems: 'center',
    },
    TopHandle:{
        flex:0.3,
        justifyContent:'space-between', 
        flexDirection:'row', 
        width:"90%", 
        marginTop:60,
        marginLeft:15,
        marginBottom:25
    },
    Kycu:{
        marginTop:70,
        fontWeight:'bold',
        fontSize:25,
        color:'#636363'
    },
    KycuButton:{
        fontSize:20,
        fontWeight:'bold',
        color:'#127489',
        width:'70%',
        marginLeft:'40%'
    },
    input:{
        backgroundColor: '#F3F3F3',
        borderRadius: 15,
        width:'90%',
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 15,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
      },
      button:{
        justifyContent:'center',
        alignContent:'center',
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        borderRadius: 15,
        width:Width-40,
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 100,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
    },
    textInput:{
        width:'70%',
        marginTop:10,
        fontWeight:'bold',
        fontSize:22,
        color:'#636363',
        marginLeft:20
    },

    triangle: {
            width: 0,
            height: 0,
            backgroundColor: 'transparent',
            borderStyle: 'solid',
            borderTopWidth: 0,
            borderRightWidth: 10,
            borderBottomWidth: 20,
            borderLeftWidth: 10,
            borderTopColor: 'black',
            borderRightColor: 'transparent',
            borderBottomColor: '#636363',
            borderLeftColor: 'transparent',
            transform: [
            {rotate: '-90deg'}]
    },
  });