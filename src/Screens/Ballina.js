import React from 'react';
import { StyleSheet, Text, View,Dimensions,TextInput,TouchableOpacity,ScrollView, SafeAreaView, Platform } from 'react-native';
import {Kartat} from '../Components/Kartat';
import Header from '../Header/Header';
import Search from '../Components/Search';
import Modal from '../Components/Modal';
import Kategorite from '../Components/Kategorite';

var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;



export default function Ballina({navigation}) {

    
    return (      
            <View style={styles.container}>
                <Modal />
                <Header navigation={navigation} isBack={false}/> 
                    <SafeAreaView style={styles.content}>             
                        <Text style={styles.Title}>{`Kerko \nMjekun Tuaj`}</Text>
                        <Search />
                        <Text style={styles.Title}>Kategoritë</Text> 
                        <View>
                            
                            <Kategorite />
                        </View> 
                        <Kartat />                                   
                  </SafeAreaView>                  
            </View>           
      );

}
const styles = StyleSheet.create({
    container: {
    flex: 1,
    width:Width,
    height:Height,
    backgroundColor: '#fff',
    alignItems: 'center',
    alignContent:'center'
    },
    content:{
        width:"90%",
        height:Platform.OS === 'android' ? '90%':'95%',
        
    },
    Title:{
        fontSize:35,
        fontWeight:'bold',
        color:'#636363',               
    },

  });