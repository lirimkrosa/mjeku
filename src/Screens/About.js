import React from 'react';
import { StyleSheet, Text, View,Dimensions,TextInput,Image,KeyboardAvoidingView} from 'react-native';
import {ScrollView, TouchableOpacity } from 'react-native-gesture-handler';
import Global from '../Components/Global'
var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;

export default function About({navigation}) {
    const context = React.useContext(Global);

    const sendMessage = () => {
        
        context.setNotification({type: 'SUCCESS', message:'Mesazhi u dergua me sukses'})
    }
    return (
        <KeyboardAvoidingView style={styles.container} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
            <ScrollView style={styles.scrollable}>
            <TouchableOpacity style={styles.triangle} onPress={() => navigation.goBack()}></TouchableOpacity>            
                <View style={styles.TopHandle}> 
                <View>
                    <Image style={{width: 200, height: 190}} source={{uri: 'https://i.imgur.com/lty6JXX.png'}}/>  
                    </View>
                </View>
                <KeyboardAvoidingView style={{width:'100%', marginLeft:'10%', flex:0.1}} behavior={Platform.OS === 'ios' ? 'padding' : undefined}>
                    <Text style={styles.textInput}>E-MAIL</Text>
                    <TextInput  style={styles.input} placeholder="psh. emri@domain.com" onChangeText={null}></TextInput>
                    <Text style={styles.textInput}>MESAZHI</Text>
                    <TextInput  style={styles.mesazhiInput} placeholder="Trego Sygjerimin apo Problemin" onChangeText={null}></TextInput>
                    <TouchableOpacity style={styles.button} onPress={() => sendMessage()}>
                        <Text style={styles.Dergo}>DERGO</Text>
                    </TouchableOpacity> 
                </KeyboardAvoidingView>
            </ScrollView>
        </KeyboardAvoidingView>
      );

}
const styles = StyleSheet.create({
    scrollable:{width:'100%'},
    container: {
    width:Width,
    height:Height,
      backgroundColor: '#fff',
      alignItems: 'center',
    },
    TopHandle:{
        marginLeft:'20%',
        marginTop:0,
        marginBottom:25,
        justifyContent:'center',
        alignContent:'center'
    },
    Dergo:{
        fontSize:20,
        fontWeight:'bold',
        color:'#127489',
        width:'70%',
        marginLeft:'37%',
        marginTop:10
    },
    input:{
        marginLeft:'-5%',
        backgroundColor: '#F3F3F3',
        borderRadius: 15,
        width:'90%',
        height:45,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 15,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
      },
      button:{
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        marginLeft:'-5%',
        borderRadius: 15,
        width:'90%',
        height:50,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 15,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
    },
    textInput:{
        
        width:'70%',
        marginTop:10,
        fontWeight:'bold',
        fontSize:22,
        color:'#636363',
        marginLeft:20
    },
    mesazhiInput:{
        marginLeft:'-5%',
        backgroundColor: '#F3F3F3',
        borderRadius: 15,
        width:'90%',
        height:150,
        paddingHorizontal: 25,
        paddingVertical:7,
        marginTop: 15,
        borderColor: 'white',
        marginBottom:15,
        fontSize:17,
        fontWeight:'bold',
    },

    triangle: {
            marginTop:50,
            marginLeft:20,
            width: 0,
            height: 0,
            backgroundColor: 'transparent',
            borderStyle: 'solid',
            borderTopWidth: 0,
            borderRightWidth: 10,
            borderBottomWidth: 20,
            borderLeftWidth: 10,
            borderTopColor: 'black',
            borderRightColor: 'transparent',
            borderBottomColor: '#636363',
            borderLeftColor: 'transparent',
            transform: [
            {rotate: '-90deg'}]
    },
  });