import React from 'react';
import { StyleSheet, Text, View, Dimensions } from 'react-native';
var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;
export default function Footer() {

    return (
      <View style={styles.container}>
      <View style={{justifyContent:'space-between', flexDirection:'row', width:"90%"}}>
        <Text>HOME</Text>
        <Text>Log out</Text>
      </View>
    </View>
      );

}
const styles = StyleSheet.create({
    container: {
      position: 'absolute', left: 0, right: 0, bottom: 0,
      width:Width,
      height:100,
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
  });