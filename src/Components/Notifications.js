import React from 'react';
import {
  Animated,
  LayoutAnimation,
  SafeAreaView,
  Text,
  TouchableOpacity,
  View,
  Dimensions,
  StyleSheet
} from 'react-native';
import Global from './Global';
const Notifications = ({notification, onPress}) => {
    const [slideInLeft, setSlideInLeft] = React.useState(new Animated.Value(0));
    const context = React.useContext(Global);

    const resetNotification = () => {
        context.setNotification({
          message: null,
          type: null,
        });
      };
    function start() {
      LayoutAnimation.easeInEaseOut();
      return Animated.parallel([
        Animated.timing(slideInLeft, {
          toValue: 1,
          duration: 300,
          useNativeDriver: true,
        }),
      ]).start();
    }
  
    function stop() {
        resetNotification()
      return Animated.parallel([
        Animated.timing(slideInLeft, {
          toValue: 0,
          duration: 100,
          useNativeDriver: true,
        }),
      ])
    }
  
    React.useEffect(() => {
      start();
      setTimeout(() => {
        stop();
      }, 1000);
      return;
    }, []);
    let transform = [
      {
        translateY: slideInLeft.interpolate({
          inputRange: [0, 1],
          outputRange: [-80, 0],
        }),
      },
    ];
  
    return (
        <SafeAreaView style={styles.container}>
          <Animated.View style={[styles.content, {transform: transform}]}>           
                <TouchableOpacity
                    onPress={onPress}
                    style={[
                      styles.messageContainer,
                      {backgroundColor: notification.type == NotificationTypes.DANGER ? Colors.pink : Colors.green, styles: Colors.white},
                    ]}>
                  <Text
                      style={{color: Colors.lightGrey, ...styles.messageText}}>{notification.message}</Text>
                </TouchableOpacity>
          </Animated.View>
        </SafeAreaView>
    );
  };
  

export default Notifications;



const width = Dimensions.get('window').width;
const styles =  StyleSheet.create({
  container: {
    height: 80,
    position: 'absolute',
    zIndex: 2,
    width: width,
    top: 0,
    backgroundColor: 'transparent',
    elevation: 1,
    paddingTop: 0,
  },

  content: {
    justifyContent: 'center',
    alignItems: 'center',
    height: '100%',
  },
  messageContainer: {
    backgroundColor: 'white',
    borderRadius: 180,
    paddingVertical: 10,
    paddingHorizontal: 20,
    alignItems: 'center',
    width: width - 60,
    borderColor: 'transparent',
    borderWidth: 1,
  },
  messageText: {
    fontWeight: 'bold',
  },
});

const Colors = {
    pink: '#FF7171',
    white: '#FFF',
    black: '#000',
    transparentBlack: 'rgba(0, 0, 0, 0.5)',
    lightGrey: '#FFF',
    darkPink: '#ef5656',
    green: '#1ccb74'
  };
  

  const NotificationTypes = {
    DANGER: 'DANGER',
    SUCCESS: 'SUCCESS',
  };