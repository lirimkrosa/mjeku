import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createStackNavigator} from '@react-navigation/stack';
import {createDrawerNavigator} from '@react-navigation/drawer';
import Ballina from '../Screens/Ballina';
import Login from '../Screens/Login';
import Register from '../Screens/Signup';
import About from '../Screens/About';
import Profile from '../Screens/Profile';
import DrawerContent from './DrawerContent'
const Menu = createDrawerNavigator();

function DefaultMenu() {
    return (
        <Menu.Navigator initialRouteName="Ballina"  drawerContent={props => <DrawerContent {... props}/>}>
            <Menu.Screen name="Ballina" component={Ballina} options={{}} />
            <Menu.Screen name="Login" component={Login} options={{}} />
            <Menu.Screen name="Register" component={Register} options={{}} />
            <Menu.Screen name="Me Shume" component={About} options={{}} />
        </Menu.Navigator>
        
    );
}



const Stack = createStackNavigator();

export default function AppNavigation() {
    return (
        <NavigationContainer >
            <Stack.Navigator headerMode={"none"} >
               <Stack.Screen name={"Menu"}  component={DefaultMenu} />
               <Stack.Screen name={"Login"}  component={Login} />
               <Stack.Screen name={"Profile"}  component={Profile} />

            </Stack.Navigator>
        </NavigationContainer>
    );
}