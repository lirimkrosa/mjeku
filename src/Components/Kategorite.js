import React, { useState } from 'react';
import {StyleSheet,Text,ScrollView,Dimensions,View,FlatList} from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Global from './Global';

var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;

export default function Kategorite({navigation}) {

    
    const context = React.useContext(Global);

    function Search(input){
        
        let njudejta = context.backupOrdinancat;
        
        njudejta = njudejta.filter(item => {      
        const itemData = `${item.kategoria}   
        ${item.kategoria} ${item.kategoria}`;
        
        const textData = input;
            
        return itemData.indexOf(textData) > -1;    
        });
        context.setOrdinancat(njudejta);  
    }


    const Select = (index, selected, label) => {
        const categories = context.kategorite.map((item) => {
          item.selected = false;
          return item;
        });
        if (selected == true)
        {categories[index].selected = false; Search('')}
        else
        {categories[index].selected = true; Search(label)}

        context.setKategorite(categories);
      };

    const renderItem = ({item}) => {
        return (   
        <View style={{marginTop:10,marginBottom:20}}>
            <TouchableOpacity style={item.selected?styles.Background : null} onPress={() => Select(item.id, item.selected, item.label)}>
                <Text style={styles.KategoriaText}>{item.label}</Text>
            </TouchableOpacity>
        </View>
    )}   



    return (
        <FlatList
        horizontal={true}
        initialNumToRender={7} 
        maxToRenderPerBatch={3}
        data={context.kategorite}
        renderItem={renderItem}
        keyExtractor={item => item.id.toString()}
        />
    );
       
}
const styles = StyleSheet.create({
    
    KategoriaText:{
        marginTop:5,
        marginLeft:20,
        marginRight:20,
        fontSize:18,
        fontWeight:'bold',
        color:'#127489',
    },
    Background:{
        marginLeft:5,
        height: 30, 
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        borderRadius:20,
        
      },
      CardTop:{
        justifyContent:'space-between', 
        flexDirection:'row', 
        width:"90%", 
        marginTop:20,
        marginLeft:15,
        marginBottom:25
      },
    });