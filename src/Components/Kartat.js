
import React, { useState } from 'react';
import { StyleSheet, Text, View,TouchableOpacity, RefreshControl,FlatList,Dimensions} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Global from './Global';
var Height = Dimensions.get('window').height;


export function Kartat() {

    const context = React.useContext(Global);

    const openModal = (emri,rruga,tel,latitude,longitude,kategoria) => {
        context.setModal(true), 
        context.setEmri(emri), 
        context.setRruga(rruga), 
        context.setTel(tel)
        context.setCoordinates({latitude,longitude})
        context.setKategoria(kategoria)
    }
    const EmptyListMessage = () => {
        
        return (
                <View style={styles.noData}>
                    <Text>Nuk u gjete asnje informacion</Text>
                </View>      

        );
    };
    const renderItem = ({item}) => {
        return (  
          <View style={{justifyContent:"center", alignContent:'center'}}>      
          <TouchableOpacity style={styles.cardBackground} onPress={() => openModal(item.emri,item.rruga, item.tel,item.latitude,item.longitude,item.kategoria)}>
              <View style={styles.CardTop}>
                  <Text style={styles.CardRrugaText} numberOfLines={2} ellipsizeMode='tail'>{item.rruga}</Text>
                  <Icon name="hospital-o" style={styles.icon}></Icon>
              </View>
              <Text style={styles.EmriText} ellipsizeMode="tail"numberOfLines={2} >{item.emri}</Text>
          </TouchableOpacity>
        </View>
        )}


        return(
            <FlatList
            refreshControl={ <RefreshControl />}
            initialNumToRender={7} 
            maxToRenderPerBatch={3}
            data={context.ordinancat}
            renderItem={renderItem}
            ListEmptyComponent={EmptyListMessage}
            keyExtractor={item => item.id.toString()}

            />
        )
}



const styles = StyleSheet.create({

    noData:{
        marginTop:'20%',
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
  
      cardBackground:{
        position:'relative',
        height: 130, 
        width: "100%", 
        backgroundColor: 'rgba(15, 155, 186, 0.3)',
        marginBottom:20,
        borderBottomRightRadius:30,
        borderTopLeftRadius:30
        
      },
      CardTop:{
        justifyContent:'space-between', 
        flexDirection:'row', 
        width:"90%", 
        marginTop:20,
        marginLeft:15,
        marginBottom:25
      },
      scrollHeight:{
          height:Height
      },
      CardRrugaText:{
        position:'relative',
        flex:1,
        marginBottom:20,
        marginRight:20,
        fontSize:15,
        fontWeight:'bold',
        color:'#127489',
        width:'70%'
      },
      EmriText:{
        fontSize:25,
        fontWeight:'bold',
        color:'#127489',
        marginLeft:15,
        width:"70%",
        marginTop:"-15%"
      },
      icon: {
        color: "#127489",
        fontSize: 70,
        marginTop: -10
    },

      
    
  });
