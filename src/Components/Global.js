import React from 'react';


const Global = React.createContext({
    user:null,
    token:null,
    modal:false,
    ordinancat:[],
    backupOrdinancat:[],
    kategorite:[],
    kategoria:null,
    notification: 
    {
        show: false,
        type: null,
        message:null
    },
    emri: null,
    rruga: null,
    tel: null,
    coordinates:
    {
        latitude: null,
        longitude: null
    },
    

    setUser: user => {},
    setToken: token => {},
    setModal: modal => {},
    setOrdinancat: ordinancat => {},
    setBackupordinancat: backupOrdinancat => {},
    setKategorite: kategorite => {},
    setEmri: emri => {},
    setRruga: rruga => {},
    setCoordinates: coordinates => {latitude,longitude},
    setTel: tel =>{},
    setKategoria: kategoria =>{},
    setNotification: ({type,message}) => {}
});

export default Global;
