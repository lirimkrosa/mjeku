import React from 'react';
import Global from './Global';
import { StyleSheet,TextInput } from 'react-native';


export default function Search() {

    const context = React.useContext(Global);

    function Search(input){
        
        let njudejta = context.backupOrdinancat;
        
        njudejta = njudejta.filter(item => {      
        const itemData = `${item.emri.toUpperCase()}   
        ${item.emri.toUpperCase()} ${item.emri.toUpperCase()}`;
        
        const textData = input.toUpperCase();
            
        return itemData.indexOf(textData) > -1;    
        });
        context.setOrdinancat(njudejta);  
    }
    

    return(
      <TextInput  style={styles.input} placeholder="Kerko" onChangeText={input => Search(input)}></TextInput>
    )
};

const styles = StyleSheet.create({
 
  input:{
      backgroundColor: '#F3F3F3',
      borderRadius: 10,
      height:40,
      paddingHorizontal: 25,
      paddingVertical:7,
      marginTop: 15,
      borderColor: 'white',
      marginBottom:15,
      fontSize:17,
      fontWeight:'bold',
    }
});