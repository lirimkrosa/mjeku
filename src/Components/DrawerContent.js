import { DrawerContentScrollView } from '@react-navigation/drawer';
import React, { useState } from 'react';
import {View,Text,StyleSheet,TouchableOpacity} from 'react-native'

export default function DrawerContent(props) {
const navigation = props.navigation;
return (
            <View style={{flex:1,marginLeft:'10%', width:'80%', height:'100%'}}>
                <DrawerContentScrollView>
                    <Text style={styles.MenuTexT}>MENU</Text>
                    <View style={{marginTop:30, flex:3}}>
                        <TouchableOpacity onPress={() => navigation.navigate('Ballina')}>
                            <Text style={styles.DefaultMenu}>BALLINA</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Login')}>
                            <Text style={styles.DefaultMenu}>KYCU</Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => navigation.navigate('Register')}>
                            <Text style={styles.DefaultMenu}>REGJISTROHU</Text>
                        </TouchableOpacity>
                    </View>
                </DrawerContentScrollView>
                <View style={{marginBottom:'20%'}}>
                    <TouchableOpacity onPress={() => navigation.navigate('Me Shume')}>
                        <Text style={styles.BottomNav}>ME SHUME</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );

}

const styles = StyleSheet.create({

    MenuTexT:{
        fontSize:40,
        fontWeight:'bold',
        color:'#636363',
    },
    DefaultMenu:{
        marginBottom:25,
        marginTop:25,
        fontSize:25,
        fontWeight:'700',
        color:'#127489',
    },
    BottomNav:{
        marginBottom:15,
        fontSize:25,
        fontWeight:'bold',
        color:'#127489',
        
    }


})