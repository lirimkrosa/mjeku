import Global from './Global';
import React, {useState} from 'react';
import Notifications from './Notifications'


const Provider = (props) => {

    const [loading, setLoading] = useState(false);
    const [user, setUser] = useState(null);
    const [token, setToken] = useState(null);
    const [modal, setModal] = useState(false);
    const [emri, setEmri] = useState(null)
    const [rruga, setRruga] = useState(null)
    const [tel, setTel] = useState(null) 
    const [kategoria, setKategoria] = useState(null)  
 

    const [coordinates, setCoordinates] = useState({
      latitude: 0,
      longitude: 0
    });
    const [notification, setNotification] = useState({
      message: null,
      type: null,
    });

    const resetNotification = () => {
      setNotification({
        message: null,
        type: null,
      });
    };

    const [ordinancat, setOrdinancat] = useState([
        {id:0,rruga:'Dëshmorët e Kombit, Ferizaj', emri:'Klinika Dentare Dhembi', kategoria:'Stomatologji',tel:'+38344897005', latitude:42.3685931, longitude:21.1522822},
        {id:1,rruga:'Rr. Ramadan Rexhepi, Ferizaj', emri:'Qendra Diagnostike Medina', kategoria:'ORL',tel:'+38345656065', latitude:42.3685931, longitude:-21.1522822},
        {id:2,rruga:'Rr. Vëllezerit Gervalla, Ferizaj', emri:'Poliklinika "Swiss Plus',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
        {id:3,rruga:'Rr Vëllezerit Gërvalla, Ferizaj', emri:'ORTODONTI', kategoria:'Stomatologji', tel:'+38344881407', latitude:'42.3685931', longitude:21.1522822},
        {id:4,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
        {id:5,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
        {id:6,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:2.3685931, longitude:21.1522822},
        {id:7,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
        {id:8,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:1.1522822},
        {id:9,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
        {id:10,rruga:'Rr. Ahmet Kaqiku', emri:'Dental House', tel:'+38345656065', latitude:42.3685931, longtitute:21.1522822}
    ]);
    const [backupOrdinancat, setBackupOrdinancat] = useState([
      {id:0,rruga:'Dëshmorët e Kombit, Ferizaj', emri:'Klinika Dentare Dhembi', kategoria:'Stomatologji',tel:'+38344897005', latitude:42.3685931, longitude:21.1522822},
      {id:1,rruga:'Rr. Ramadan Rexhepi, Ferizaj', emri:'Qendra Diagnostike Medina', kategoria:'ORL',tel:'+38345656065', latitude:42.3685931, longitude:-21.1522822},
      {id:2,rruga:'Rr. Vëllezerit Gervalla, Ferizaj', emri:'Poliklinika "Swiss Plus',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
      {id:3,rruga:'Rr Vëllezerit Gërvalla, Ferizaj', emri:'ORTODONTI', kategoria:'Stomatologji', tel:'+38344881407', latitude:'42.3685931', longitude:21.1522822},
      {id:4,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
      {id:5,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
      {id:6,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:2.3685931, longitude:21.1522822},
      {id:7,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
      {id:8,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:1.1522822},
      {id:9,rruga:'Ordinanca Specialistike Urologjike "Pro-R', emri:'Ordinanca Specialistike Urologjike "Pro-R',kategoria:'', tel:'+38345656065', latitude:42.3685931, longitude:21.1522822},
      {id:10,rruga:'Rr. Ahmet Kaqiku', emri:'Dental House', tel:'+38345656065', latitude:42.3685931, longtitute:21.1522822}
    ]);
    const [kategorite, setKategorite] = useState([
      {id: 0, label:'ORL',value:'ORL', selected:false},
      {id: 1, label:'Stomatologji', value:'Stomatologji',selected:false},
      {id: 2, label:'Pediatri',value:'Pediatri', selected:false},
      {id: 3, label:'Fizioterapi', value:'Fizioterapi',selected:false},
      {id: 4, label:'Gjinekologji',value:'Gjinekologji', selected:false},

    ])


    return (
        <Global.Provider value={{
          user: user,
          token: token,
          modal: modal,
          ordinancat: ordinancat,
          backupOrdinancat: backupOrdinancat,
          emri: emri,
          rruga: rruga,
          tel: tel,
          notification:notification,
          kategorite:kategorite,
          coordinates: coordinates,
          kategoria:kategoria,
          setKategorite: kategorite => setKategorite(kategorite),
          setToken: token => setToken(token),
          setUser: user => setUser(user),
          setModal: modal => setModal(modal),
          setOrdinancat: ordinancat => setOrdinancat(ordinancat),
          setBackupOrdinancat: backupOrdinancat => setBackupOrdinancat(backupOrdinancat),
          setEmri: emri => setEmri(emri),
          setRruga: rruga => setRruga(rruga),
          setTel: tel => setTel(tel),
          setNotification: ({type,message}) => setNotification({type,message}),
          setCoordinates: ({latitude, longitude}) => setCoordinates({latitude,longitude}),
          setKategoria: kategoria => setKategoria(kategoria),
        }}>

        {
        notification.message != null ? 
          <Notifications onPress={() => resetNotification()} notification={notification}/> 
        : null
        }
  
          {props.children}
        </Global.Provider>
    );
  };
  
  export default Provider;