import React from 'react';
import {StyleSheet,Text, View,TouchableOpacity, Linking} from 'react-native';
import SwipablePanel from '../Modal/Panel';
import Global from './Global';
import Icon from 'react-native-vector-icons/FontAwesome';
import Fontisto from 'react-native-vector-icons/Fontisto';
import MapView, { Marker } from 'react-native-maps';


export default function Modal({navigation}) {

    const context = React.useContext(Global);
    const closePanel = () => {context.setModal(false)};

    const call = (type) => {
        let thirre;
        if (type == 'phone'){ thirre =`tel:${context.tel}`}
        else if ( type == 'whatsapp'){thirre =`whatsapp://send?phone=${context.tel}`}
        else if ( type == 'viber'){thirre =`viber://contact?number=${context.tel}`}
        return Linking.openURL(thirre);     
    }
    return (
            <SwipablePanel closeOnTouchOutside noBar showCloseButton onlyLarge onClose={closePanel} onPressCloseButton={closePanel} isActive={context.modal}>
                  <MapView style={styles.map}
                            initialRegion={{
                            latitude: context.coordinates.latitude,
                            longitude: context.coordinates.longitude,
                            latitudeDelta: 0.005,
                            longitudeDelta: 0.005,}}>
                            <Marker coordinate = {{latitude: context.coordinates.latitude,longitude: context.coordinates.longitude}}
                                    pinColor = {"purple"}
                                    title={context.emri}
                                    description={context.kategoria}/>
                   </MapView>
                <View style={{marginTop:'80%'}}>
                <View style={styles.infoHandle}>
                    <Icon name="hospital-o" style={styles.icon}></Icon>
                    <Text style={styles.text}>{context.emri}</Text>
                </View>
                <View style={styles.infoHandle}>
                    <Icon name="map-pin" style={styles.icon}></Icon>
                    <Text style={styles.text}>{context.rruga}</Text>
                </View>
                <View style={styles.callHandle}>
                    <TouchableOpacity onPress={() => call('phone')}>
                        <Icon name="phone" style={styles.phone}></Icon>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => call('whatsapp')}>
                        <Icon name="whatsapp" style={styles.whatsapp}></Icon>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => call('viber')}>
                        <Fontisto name="viber" style={styles.viber}></Fontisto>
                    </TouchableOpacity>
                    </View>
                </View>
            </SwipablePanel>     
       );
       
}

const styles = StyleSheet.create({
    map: {
        position: 'absolute',
        width:'100%',
        height:'45%',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        borderRadius:20
      },
    infoHandle:{
        flexDirection:'row', 
        width:"90%", 
        marginTop:20,
        marginLeft:15,
        marginBottom:25
    },
    callHandle:{
        justifyContent:'space-between', 
        flexDirection:'row', 
        width:"90%", 
        marginTop:50,
        marginLeft:15,
        marginBottom:25
    },
    text:{
        marginLeft:"15%",
        fontSize:20,
        fontWeight:'bold',
        color:'#636363',
        width:"70%",
        marginTop:10
    },
    icon: {
        color: "#127489",
        fontSize: 60,
        marginTop: -10
    },
    phone: {
        color: "#127489",
        fontSize: 70,
        marginTop: -6
    },
    whatsapp: {
        color: "#1ccb74",
        fontSize: 70,
        marginTop: -10
    },
    viber: {
        color: "#6F13A8",
        fontSize: 64,
        marginTop: -8
    }
});
