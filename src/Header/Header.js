import React from 'react';

import { StyleSheet, Text, View,Dimensions, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

var Width = Dimensions.get('window').width;
var Height = Dimensions.get('window').height;

export default function Header({navigation}) {

    return (
        <View style={styles.container}>
          <View style={styles.header}>
            <TouchableOpacity  onPress={() => navigation.toggleDrawer()}>
              <View style={styles.line1}></View>
              <View style={styles.line1}></View>
              <View style={styles.line2}></View>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => navigation.push('Profile')}>
              <Icon name="cog" style={styles.icon}></Icon>
            </TouchableOpacity>
          </View>
        </View>
      );

}
const styles = StyleSheet.create({
    container: {
      width:Width,
      height:"9%",
      backgroundColor: '#fff',
      alignItems: 'center',
      justifyContent: 'center',
    },
    line1:{
      marginTop:3,
      height: 4, 
      width: 25, 
      backgroundColor: '#636363',
      borderRadius:10
    },
    line2:{
      marginTop:3,
      height: 4, 
      width: 18, 
      backgroundColor: '#636363',
      borderRadius:10
    },
    header:{
      marginTop:25,
      justifyContent:'space-between', 
      alignContent:'space-around',
      flexDirection:'row', 
      width:"90%", 
    },
    icon: {
      color: "#636363",
      fontSize: 25,
  },
  });